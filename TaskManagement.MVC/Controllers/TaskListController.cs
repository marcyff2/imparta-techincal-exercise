﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TaskManagement.MVC.Controllers
{
        public class TaskListController : Controller
        {
            [HttpGet]
            public ActionResult Index()
            {
                Models.TaskListViewModel Model = new Models.TaskListViewModel();
                return View(Model);
            }

            [HttpPost]
            public void DeleteTask(int id)
            {
                Models.TaskViewModel Model = new Models.TaskViewModel(id);
                Model.Delete();
            }
            [HttpPost]
            public ActionResult SaveTask(Models.TaskViewModel Model)
            {
                Model.Update();
                return Json(new { IsDone = Model.IsDone, Name = Model.Name, id = Model.Id });
            }

        }
    }
