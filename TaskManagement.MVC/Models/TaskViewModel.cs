﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagement.MVC.Models
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDone { get; set; }
        public TaskViewModel()
        {

        }
        public TaskViewModel(int id)
        {
            Id = id;
        }

        public void Delete()
        {
            TaskListLogic.DeleteTask(Id);
        }
        public void Update()
        {
            Id = TaskListLogic.Save(Id, Name, IsDone, HttpContext.Current.User.Identity.Name);
        }
    }
}