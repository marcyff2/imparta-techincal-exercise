﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagement.MVC.Models
{
    public class TaskListViewModel
    {
        public List<TaskViewModel> TaskList { get; set; }

        public TaskListViewModel()
        {
            var user = HttpContext.Current.User.Identity.Name;
            ITaskListLogic logic = new TaskListLogic(user);
            logic.InitialiseUser();
            TaskList = new List<TaskViewModel>();
            foreach (var task in logic.ListTasks)
            {
                TaskList.Add(new TaskViewModel() { Id = task.TaskId, Name = task.TaskName, IsDone = task.Done });
            }
        }


    }
}