﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TaskManagement.EF;

namespace TaskManagement.Repositories
{
    public class TaskRepository : ITaskRepository
    {

        private readonly TaskManagementContext context;

        public TaskRepository()
        {
            context = new TaskManagementContext();
        }

        public void Add(UserTask entity)
        {
            context.UserTasks.Add(entity);
        }

        public IEnumerable<UserTask> Find(Expression<Func<UserTask, bool>> predicate)
        {
            return context.UserTasks.Where(predicate).ToList();
        }

        public UserTask Get(int Id)
        {
            return context.UserTasks.FirstOrDefault(x=>x.TaskId == Id);
        }

        public IEnumerable<UserTask> GetAll()
        {
            return context.UserTasks.ToList();
        }

        public IEnumerable<UserTask> GetByUser(string user)
        {
            return context.UserTasks.Where(x => x.User == user).ToList();
        }

        public void Update(int id, UserTask task)
        {
            var dbTask = context.UserTasks.FirstOrDefault(x => x.TaskId == id);
            dbTask.TaskName = task.TaskName;
            dbTask.Done = task.Done;
            dbTask.User = task.User;
        }
        public void Save()
        {
            context.SaveChanges();
        }

        public void Remove(UserTask entity)
        {
            context.UserTasks.Remove(entity);
        }

       
    }
}
