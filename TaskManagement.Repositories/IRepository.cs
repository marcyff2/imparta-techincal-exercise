﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TaskManagement.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(int Id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);

        void Remove(TEntity entity);
       void Update(int id, TEntity entity);

        void Save();



    }
}