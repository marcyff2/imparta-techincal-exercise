﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.EF;

namespace TaskManagement.Repositories
{
    public interface ITaskRepository : IRepository<UserTask>
    {
        IEnumerable<UserTask> GetByUser(string user);
    }
}