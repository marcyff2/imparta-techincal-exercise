﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Repositories;
using TaskManagement.EF;

namespace Business
{
    public interface ITaskListLogic
    {
        string User { get; set; }
        ITaskRepository Repo {get;set;}
        IEnumerable<UserTask> ListTasks { get; set; }
        void InitialiseUser();
        void GetAllTasksByUser();
    }
}
