﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Repositories;
using TaskManagement.EF;

namespace Business
{
    public class TaskListLogic : ITaskListLogic
    {
        public string User { get; set; }
        public ITaskRepository Repo { get; set; }
        public IEnumerable<UserTask> ListTasks { get; set; }

        public TaskListLogic(string User)
        {
            this.User = User;
            ListTasks = new List<UserTask>();
            Repo = new TaskRepository();

        }

        protected void GenerateTasks()
        {
            AddTask("1st Task", false);
            AddTask("2nd Task", false);
            AddTask("3rd Task", false);
            AddTask("4th Task", false);
            AddTask("5th Task", false);
        }

        public void GetAllTasksByUser()
        {
            ListTasks = Repo.GetByUser(User);
        }

        public void AddTask(string name , bool Done)
        {
            Repo.Add(new UserTask
            {
                TaskName = name,
                Done = Done,
                User = User
            });
            Repo.Save();

        }

        public void InitialiseUser()
        {
            GetAllTasksByUser();
            if(ListTasks == null || ListTasks.Count() == 0)
            {
                GenerateTasks();
                GetAllTasksByUser();
            }
        }


        public static int Save(int id, string name, bool isDone, string user)
        {
            ITaskRepository Repo = new TaskRepository();
            var Task = Repo.Get(id);
            if (Task == null)
                Task = new UserTask();
            Task.TaskName = name;
            Task.Done = isDone;
            Task.User = user;
            if (id == 0)
                Repo.Add(Task);
            else
                Repo.Update(id, Task);
            Repo.Save();
            return Task.TaskId;
        }

        public static void DeleteTask(int id)
        {
            ITaskRepository Repo = new TaskRepository();
            var Task = Repo.Get(id);
            Repo.Remove(Task);
            Repo.Save();
        }


    }
}
