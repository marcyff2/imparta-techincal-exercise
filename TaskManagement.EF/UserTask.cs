﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagement.EF
{
    public class UserTask
    {
        [Key]
        public int TaskId { get; set; }
        public string User { get; set; }
        public string TaskName { get; set; }
        public bool Done { get; set; }
    }
}
