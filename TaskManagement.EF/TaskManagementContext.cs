﻿using System;
using System.Collections.Generic;
using System.Data.Entity;


namespace TaskManagement.EF
{
    public class TaskManagementContext : DbContext
    {
        public TaskManagementContext() : base()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<TaskManagementContext>());
        }
        public DbSet<UserTask> UserTasks { get; set; }
    }
}
